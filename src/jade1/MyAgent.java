
package jade1;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import static jade.lang.acl.ACLMessage.*;
import jade.lang.acl.UnreadableException;
import java.util.ArrayList;

/**
 * @author Bartosz Hajdaś
*/
public class MyAgent extends Agent 
{
    public enum STATES {ASK_FORK, WAIT_FOR_ANSWER, ASK_FORK2, WAIT_FOR_ANSWER2, 
                        REQUEST_CONSUME, CONSUME, CLEAR_FORKS, DODELETE };
    private int m_iTime = 0;
    private AID senderID = null;
    private AID leftForkID = null;
    private AID rightForkID = null;
    private AID takenForkID = null;
    STATES state = STATES.ASK_FORK;
    ArrayList<MyKebab> myTokenList = new ArrayList(); 
    ACLMessage msg = null;
    
    
    protected void setup() 
    {
        MyLogger.log(this, "Wstałem : "+ getLocalName(), MyLogger.PRIORITY.MEDIUM);
        
        Object[] args = getArguments();
        
        if(null != args)
        {
            if(4 <= args.length )
            {
                try
                {
                    m_iTime = (int)args[0];
                    senderID = (AID)args[1];
                    leftForkID = (AID)args[2];
                    rightForkID = (AID)args[3];
                    MyLogger.log(this, "LeftFork : "+ leftForkID.getLocalName() + " RightFork : " + rightForkID.getLocalName() , MyLogger.PRIORITY.LOW);
                }
                catch (Exception ex)
                {
                    doDelete();
                }                
            }
            else
            {
                doDelete();
                throw new NullPointerException("Wrong arguments!!!");            
            }
        }
        else
        {
            doDelete();
            throw new NullPointerException("Wrong arguments!!!");
        }
        
        addBehaviour(new TickerBehaviour(this, m_iTime) 
        {
            AID forkId = null;
            
            @Override
            protected void onTick() 
            {
                switch(state)
                {
                    case ASK_FORK:
                        forkId = ((StaticRandom.getNextUInt() % 2) == 1) ? leftForkID : rightForkID;
                        msg = new ACLMessage(ACLMessage.PROPOSE);
                        msg.addReceiver(forkId);
                        state = STATES.WAIT_FOR_ANSWER;
                        MyLogger.log(myAgent, "Wysyłam prosbe o fork do " + forkId.getLocalName() , MyLogger.PRIORITY.HIGH);
                        send(msg);                        
                        break;
                    
                    case WAIT_FOR_ANSWER:
                        msg = receive();
                        if(null != msg)
                        {
                            switch(msg.getPerformative())
                            {
                                case REFUSE:
                                    state = STATES.ASK_FORK;    
                                    MyLogger.log(myAgent, "Odrzucono prosbe o fork od " + msg.getSender().getLocalName() , MyLogger.PRIORITY.HIGH);
                                    break;
                                    
                                case ACCEPT_PROPOSAL:
                                    state = STATES.ASK_FORK2;
                                    takenForkID = msg.getSender();
                                    MyLogger.log(myAgent, "Zakceptowano prosbe o fork od " + msg.getSender().getLocalName() , MyLogger.PRIORITY.HIGH);
                                    break;  
                                    
                                case DISCONFIRM:
                                    state = STATES.DODELETE;
                                    break;
                            }                            
                        }
                        break;
                        
                    case ASK_FORK2:
                        
                        forkId = (takenForkID.getName().equalsIgnoreCase(leftForkID.getName())) ? rightForkID : leftForkID;
                        msg = new ACLMessage(ACLMessage.PROPOSE);
                        msg.addReceiver(forkId);
                        state = STATES.WAIT_FOR_ANSWER2;
                        MyLogger.log(myAgent, "Wysyłam prosbe o drugi fork do " + forkId.getLocalName() , MyLogger.PRIORITY.HIGH);
                        send(msg);                        
                        break;

                    case WAIT_FOR_ANSWER2:
                        msg = receive();
                        if(null != msg)
                        {
                            switch(msg.getPerformative())
                            {
                                case REFUSE:
                                    state = STATES.ASK_FORK;    
                                    ACLMessage sendCancelMsg = new ACLMessage(ACLMessage.CANCEL);
                                    sendCancelMsg.addReceiver(takenForkID);
                                    send(sendCancelMsg);
                                    MyLogger.log(myAgent, "Zwalniam zasób w " + takenForkID.getLocalName() , MyLogger.PRIORITY.HIGH);
                                    break;
                                    
                                case ACCEPT_PROPOSAL:
                                    state = STATES.REQUEST_CONSUME;
                                    takenForkID = msg.getSender();
                                    MyLogger.log(myAgent, "Zakceptowano prosbe o drugi fork od " + msg.getSender().getLocalName() , MyLogger.PRIORITY.HIGH);
                                    break;    
                                    
                                case DISCONFIRM:
                                    state = STATES.DODELETE;
                                    break;
                            }                            
                        }    
                        
                        break;
                        
                    case REQUEST_CONSUME:
                        MyLogger.log(myAgent, "Wysyłam request o kebaba!" , MyLogger.PRIORITY.HIGH);
                        state = STATES.CONSUME;
                        msg = new ACLMessage(ACLMessage.REQUEST);
                        msg.addReceiver(senderID);
                        send(msg);                        
                        break;
                        
                    case CONSUME:
                        msg = receive();
                        if(null != msg)
                        {
                            switch(msg.getPerformative())
                            {
                                case CONFIRM:                                
                                    try
                                    {
                                        if(msg.getContentObject() instanceof MyKebab)
                                        {
                                            MyKebab oKebak = (MyKebab)msg.getContentObject();
                                            myTokenList.add(oKebak);
                                            MyLogger.log(myAgent, "Consumuje kebak o id = " + oKebak.getId(), MyLogger.PRIORITY.ULTRA);
                                            state = STATES.CLEAR_FORKS;    
                                        }
                                    } 
                                    catch (UnreadableException ex) 
                                    {
                                        MyLogger.log(myAgent, "Something went wrong! \n", MyLogger.PRIORITY.HIGH);
                                    }
                                    break; 
                                    
                                case CANCEL:
                                    state = STATES.CLEAR_FORKS;                            
                                    break;
                                    
                                case DISCONFIRM:
                                    state = STATES.DODELETE;
                                    break;
                            }
                        }
                        break;
                        
                    case CLEAR_FORKS:
                        MyLogger.log(myAgent, "Zwalniam zasoby w forkach!" , MyLogger.PRIORITY.HIGH);
                        state = STATES.ASK_FORK;
                        msg = new ACLMessage(ACLMessage.CANCEL);
                        msg.addReceiver(leftForkID);
                        msg.addReceiver(rightForkID);
                        send(msg);                        
                        break;
                    case DODELETE:
                        
                        MyLogger.log(myAgent, "Zjadłem " + myTokenList.size() + " kebabów!", MyLogger.PRIORITY.ULTRA);
                        for(int i = 0; i< myTokenList.size(); i++)
                        {
                            MyKebab oKebab = myTokenList.get(i);
                            MyLogger.log(myAgent, "Zjadłem kebak o nazwie = " + oKebab.getName(), MyLogger.PRIORITY.ULTRA);
                        }
                        doDelete();
                        break;
                }
            }
        });
    }
}