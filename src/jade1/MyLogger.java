package jade1;

import jade.core.Agent;

/**
 * @author Bartosz Hajdaś
 */
public class MyLogger {
    
    /**
     * loggerPriority - minimal priority to display
     */
    static PRIORITY loggerPriority = PRIORITY.HIGH;
    
    enum PRIORITY{LOW, MEDIUM, HIGH, ULTRA};
    
    static void log(Agent agent,String log, PRIORITY prio)
    {
        if(loggerPriority.ordinal() <= prio.ordinal())
        {
            System.out.println("Agent:" + agent.getName() + " *** " + log);
        }
   }     
}
