package jade1;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Bartosz Hajdaś
 */
public class Producent extends Agent 
{
    ArrayList<MyKebab> tokenList = new ArrayList();  
    int maxTokens = 100;
    ArrayList<AID> agentList = new ArrayList();
    ArrayList<AID> agentForkList = new ArrayList();
    long startTime = System.nanoTime();
    long endTime = 0;   
	
    protected void setup() 
    {
        Object[] args = getArguments();
        
        if((args != null) && (2 <= args.length))
        {
            int agentCount = Integer.parseInt((String) args[0]);
            maxTokens = Integer.parseInt((String) args[1]);
            if(0 >= maxTokens)
            {
                throw new IndexOutOfBoundsException("Wrong size");
            }
            prepareKebabs();
            
            
            AgentContainer c = getContainerController();
            try 
            {            
                for(int i = 0; i < agentCount; i++)
                {
                    String agentName = "ForkAgent" + i;
                    AgentController a = c.createNewAgent(agentName, "jade1.MyForkAgent", null);
                    a.start();    
                    agentForkList.add(new AID(agentName, false));
                    MyLogger.log(this, "+++ Created: ForkAgent" + i, MyLogger.PRIORITY.LOW);                    
                }
                
                for(int i = 0; i < agentCount; i++)
                {
                    String agentName = "Agent" + i;
                    AgentController a = c.createNewAgent(agentName, "jade1.MyAgent", prepareData(agentName,getAID(), agentForkList.get(i%agentCount), agentForkList.get((i+1)%agentCount)));
                    a.start();    
                    agentList.add(new AID(agentName, false));
                    MyLogger.log(this, "+++ Created: Agent" + i, MyLogger.PRIORITY.LOW);
                }
            } 
            catch (Exception e) 
            {
                //nothing to do
            }            
        }
        
        addBehaviour(new SimpleBehaviour(this) {
            @Override
            public void action() {
                ACLMessage msg = receive();
                if(null != msg)
                {
                    MyLogger.log(myAgent, "Otrzymalem wiadomosc od " + msg.getSender().getLocalName()+ " List size = "+ tokenList.size() , MyLogger.PRIORITY.LOW);
                    ACLMessage reply = msg.createReply();
                    
                    switch(msg.getPerformative())
                    {
                        case ACLMessage.REQUEST: 
                            try 
                            {
                                if(!tokenList.isEmpty())
                                {
                                    reply.setContentObject(tokenList.remove(0));
                                    reply.setPerformative(ACLMessage.CONFIRM);
                                    send(reply);
                                }
                                else
                                {
                                    reply.setPerformative(ACLMessage.CANCEL);
                                    send(reply);
                                }
                            } 
                            catch (IOException ex) 
                            {
                                MyLogger.log(myAgent, "Something went wrong! \n", MyLogger.PRIORITY.HIGH);
                            }   
                            break;
                    }
                }
            }

            @Override
            public boolean done() {
                boolean retVal = tokenList.isEmpty();
                if(true == retVal)
                {
                    ACLMessage msg = new ACLMessage(ACLMessage.DISCONFIRM);
                    for(int i = 0; i< agentList.size(); i++)
                    {
                        msg.addReceiver(agentList.get(i));
                    }
                    for(int i = 0; i< agentForkList.size(); i++)
                    {
                        msg.addReceiver(agentForkList.get(i));
                    }                    
                    
                    MyLogger.log(myAgent, "WYSYLAM WYLACZENIE", MyLogger.PRIORITY.HIGH);
                    send(msg);
                    
                    endTime = System.nanoTime();                    
                    MyLogger.log(myAgent, "System pracował :" + (endTime - startTime)/1000000000 + " sekund" , MyLogger.PRIORITY.HIGH); 
                    doDelete();                   
                }
                return retVal;
            }
        });
    }  

    
    private Object [] prepareData(String agentName, AID parent, AID fork1, AID fork2)
    {
        int iTime = (StaticRandom.getNextUInt() % 1000) + 500;        
        Object [] dataList = new Object[4];
        dataList[0] = iTime;
        dataList[1] = parent;
        dataList[2] = fork1;
        dataList[3] = fork2;
        MyLogger.log(this, "Dane dla agenta: " + agentName + " Czas reakcji :" + iTime + "\n Fork1:" + fork1.getLocalName() + "\n Fork2:" + fork2.getLocalName(), MyLogger.PRIORITY.ULTRA);
        
        return dataList;
    }
    
    private void prepareKebabs()
    {
        for(int i = 0; i < maxTokens; i++)
        {
            MyKebab oKebab = new MyKebab(i, "Kebab " + i);
            tokenList.add(oKebab);                    
        }    
    }
}