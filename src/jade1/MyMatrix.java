/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jade1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author Bartosz Hajdaś
 */
public class MyMatrix {

    int[][] matrix = null;
    int matrixSize;
    
    public MyMatrix(int size) {
        matrixSize = size;
        matrix = new int[matrixSize][matrixSize];                
    }
    
    public void zeros()
    {
        for(int i = 0; i <matrixSize; i++)
            for(int j = 0; j <matrixSize; j++)
                matrix[i][j] = 0;    
    }
    
    public void ones()
    {
        for(int i = 0; i <matrixSize; i++)
            for(int j = 0; j <matrixSize; j++)
                matrix[i][j] = 1;    
    }
    
    public void generateMatrix()
    {
        for(int i = 0; i <matrixSize; i++)
            for(int j = 0; j <matrixSize; j++)
                matrix[i][j] = StaticRandom.getNextInt();
    }
    
    public String getRowInString(int row)
    {
        String retVal = "";
        for(int i =0; (i < matrixSize) && (row < matrixSize); i++)
        {
            if((i + 1) < matrixSize)
            {
                retVal += matrix[row][i] + "$";
            }
            else
            {
                retVal += matrix[row][i];
            }
        }
        return retVal;
    }
    
    public String getColumnInString(int column)
    {
        String retVal = "";
        for(int i =0; (i < matrixSize) && (column < matrixSize); i++)
        {
            if((i + 1) < matrixSize)
            {
                retVal += matrix[i][column] + "$";
            }
            else
            {
                retVal += matrix[i][column];
            }
        }
        return retVal;
    }
    
    public int[][] getMatrix()
    {
        return matrix;
    }
    
    public void wypisz()
    {
        for(int i = 0; i <matrixSize; i++)
        {
            for(int j = 0; j <matrixSize; j++)
            {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    public void saveToFile(String filename)
    {
        File oFile = null;
        FileWriter oFileWriter = null;
        BufferedWriter oWriter = null;
        try
        {
            oFile = new File(filename);
            oFileWriter = new FileWriter(oFile);
            oWriter = new BufferedWriter(oFileWriter);
            
            for(int i = 0; i< matrixSize; i++)
            {
                for(int j = 0; j < matrixSize; j++)
                {
                    oWriter.write(matrix[i][j] + " ");                
                }
                oWriter.write("\n");            
            }
            
            oWriter.close();
            oFileWriter.close();                   
        }
        catch (Exception ex)
        {  
            ex.printStackTrace();
        }       
    }
}
