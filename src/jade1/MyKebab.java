/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jade1;

import java.io.Serializable;

/**
 *
 * @author Bartosz Hajdaś
 */
public class MyKebab implements Serializable {
    
    private String name;
    private int Id;
    
    public MyKebab(int a_Id, String a_Name) {
        Id = a_Id;
        name = a_Name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return Id;
    }
    
}
