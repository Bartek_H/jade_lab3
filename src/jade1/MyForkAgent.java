package jade1;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import static jade.lang.acl.ACLMessage.*;
/**
 * @author Bartosz Hajdaś
*/
public class MyForkAgent extends Agent 
{
    private boolean isAnyAgentRegistered = false;
    private AID registeredAgent = null;
    
    protected void setup() 
    {
        MyLogger.log(this, "Wstałem : "+ getLocalName(), MyLogger.PRIORITY.MEDIUM);
        
        addBehaviour(new SimpleBehaviour(this) {
            @Override
            public void action() 
            {
                ACLMessage msg = receive();
                if(null != msg)
                {                    
                    MyLogger.log(myAgent, "Otrzymalem wiadomosc od " + msg.getSender().getLocalName() , MyLogger.PRIORITY.MEDIUM);
                    ACLMessage reply = msg.createReply();
                    switch(msg.getPerformative())
                    {
                        case PROPOSE:
                            if(false == isAnyAgentRegistered)
                            {
                                MyLogger.log(myAgent, "Potwiedzam przypisanie agenta : " + msg.getSender().getLocalName() + " do widelca " + getLocalName() , MyLogger.PRIORITY.HIGH);
                                isAnyAgentRegistered = true;
                                registeredAgent = msg.getSender();
                                reply.setPerformative(ACCEPT_PROPOSAL);
                                send(reply);
                            }
                            else
                            {
                                MyLogger.log(myAgent, "Odrzucam przypisanie agenta : " + msg.getSender().getLocalName() + " do widelca " + getLocalName() , MyLogger.PRIORITY.MEDIUM);
                                reply.setPerformative(REFUSE);
                                send(reply);
                            }
                            break;
                            
                        case CANCEL:
                        {
                            if((null != registeredAgent) && (msg.getSender().getName().equalsIgnoreCase(registeredAgent.getName())) && isAnyAgentRegistered)
                            {
                                MyLogger.log(myAgent, "Zwalniam zasób agenta : " + msg.getSender().getLocalName() + " do widelca " + getLocalName() , MyLogger.PRIORITY.MEDIUM);
                                isAnyAgentRegistered = false;
                                registeredAgent = null;
                            }
                            break;
                        }
                            
                        case DISCONFIRM:
                            MyLogger.log(myAgent, "Wyłączam się !" , MyLogger.PRIORITY.HIGH);
                            doDelete();
                            break;                    
                    }
                }
            }
            
            @Override
            public boolean done() {
                return false;
            }
        });
    }    
}